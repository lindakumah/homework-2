#!/usr/bin/env python 
from nose.tools import assert_equal
import aims

def test_ints():
    numbers = [1, 2, 3, 4, 5]
    obs = aims.std(numbers)
    exp = 1.4142135623730951
    assert_equal(obs, exp)

def test_neg_ints():
    numbers = [-1, -2, -3, -4, -5]
    obs = aims.std(numbers)
    exp = 1.4142135623730951
    assert_equal(obs, exp)

def test_float():
    numbers = [0.1, 0.2, 0.3, 0.4,]
    obs = aims.std(numbers)
    exp = 0.11180339887498948
    assert_equal(obs, exp)

def test_neg_float():
    numbers = [-0.1, -0.2, -0.3, -0.4,]
    obs = aims.std(numbers)
    exp = 0.11180339887498948
    assert_equal(obs, exp)

def test_avg1():
    files = ['data/bert/audioresult-00384']
    abs = aims.avg_range(files)
    exp = 1.0
    assert_equal(abs, exp)

def test_avg2():
    files = ['data/bert/audioresult-00393']
    abs = aims.avg_range(files)
    exp = 4.0
    assert_equal(abs, exp)

def test_avg3():
    files = ['data/bert/audioresult-00470']
    abs = aims.avg_range(files)
    exp = 8.0
    assert_equal(abs, exp)
def test_avg4():
    files = ['data/bert/audioresult-00215']
    abs = aims.avg_range(files)
    exp = 5.0
    assert_equal(abs, exp)


