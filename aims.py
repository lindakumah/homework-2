#!/usr/bin/env python
import numpy as np

def std(li):
    N = len(li)
    new_li = np.array(li)
    avg = sum(new_li)/N
    diff = new_li - avg
    diff_sqr = diff**2
    sum_sqr = sum(diff_sqr)
    sta_dev = np.sqrt(sum_sqr/N)
    return sta_dev

def avg_range(lst):
    files=[]
    ranges=[]
    for location in lst:
        files.append(open(location))  # returns a list containing the lines of the file
    for entry in files:
        for line in entry:
            if 'Range' in line:
                ranges.append(float(line[7]))  # gets the value of range from the file and converts it into a float
    return sum(ranges)/len(ranges)  



def main():
    print "Welcome to AIMS module"
    print "std is ;",std([2,2])
    print avg_range(lst)
    

if __name__ == "__main__":
    main()
